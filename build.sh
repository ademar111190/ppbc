#!/usr/bin/env bash

mkdir build
cd build
cmake .. && make
make install
cd ..

echo ""
echo "Build done, lets run it:"
echo ""

./build/ppbc
